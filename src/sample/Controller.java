package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Controller implements Initializable {

    @FXML
    TableView<Mahasiswa> mahasiswaTable;

    @FXML
    TableColumn<Mahasiswa, String> nimCol;

    @FXML
    TableColumn<Mahasiswa, String> namaCol;

    @FXML
    TableColumn<Mahasiswa, Integer> semesterCol;

    @FXML
    TableColumn<Mahasiswa, String> matakuliahCol;

    @FXML
    TableColumn<Mahasiswa, Double> nilaiCol;

    @FXML
    TextField nim;

    @FXML
    TextField nama;

    @FXML
    TextField nilai;

    @FXML
    ComboBox<String> semester;

    @FXML
    ComboBox<String> matakuliah;

    @FXML
    TextField nimSearch;

    @FXML
    Text ipk;

    List<Mahasiswa> allMahasiswa = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        nimCol.setCellValueFactory(new PropertyValueFactory<Mahasiswa, String>("nim"));
        namaCol.setCellValueFactory(new PropertyValueFactory<Mahasiswa, String>("nim"));
        semesterCol.setCellValueFactory(new PropertyValueFactory<Mahasiswa, Integer>("semester"));
        matakuliahCol.setCellValueFactory(new PropertyValueFactory<Mahasiswa, String>("matakuliah"));
        nilaiCol.setCellValueFactory(new PropertyValueFactory<Mahasiswa, Double>("nilai"));

        mahasiswaTable.getColumns().setAll(nimCol, namaCol, semesterCol, matakuliahCol, nilaiCol);

        IntStream
                .rangeClosed(1, 8)
                .forEach(x -> semester.getItems().add(String.valueOf(x)));

        matakuliah
                .getItems()
                .setAll(
                    new String[]{
                        "Keputrian", "Seni Nuklir", "Analisa Atom" } );

    }

    public void onAdd(ActionEvent actionEvent) {
        Mahasiswa mhs = new Mahasiswa(
                            nim.getText(),
                            nama.getText(),
                            matakuliah.getSelectionModel().getSelectedItem(),
                            Integer.parseInt(semester.getSelectionModel().getSelectedItem()),
                            Double.parseDouble(nilai.getText()));

        mahasiswaTable.getItems().add(mhs);
        allMahasiswa.add(mhs);

        nim.clear();
        nama.clear();
        matakuliah.getSelectionModel().clearSelection();
        semester.getSelectionModel().clearSelection();
        nilai.clear();
        String ipkResult = Mahasiswa.hasilIPK(
                mahasiswaTable.getItems().stream());
        ipk.setText(ipkResult);
    }

    public void onSearching(KeyEvent keyEvent) {
        String nim = nimSearch.getText();
        ObservableList<Mahasiswa> items = mahasiswaTable.getItems();
        if(nim.equals(""))
            items.setAll(allMahasiswa);
        else
            mahasiswaTable
                .getItems()
                .setAll(allMahasiswa
                        .stream()
                        .filter(x -> x.getNim().equals(nim))
                        .collect(Collectors.toList()));

        String resultIpk = Mahasiswa.hasilIPK(mahasiswaTable.getItems().stream());
        ipk.setText(resultIpk);
    }
}
