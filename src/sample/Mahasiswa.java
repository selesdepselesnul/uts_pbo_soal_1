package sample;


import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class Mahasiswa extends Matakuliah {
    private String nim;
    private String nama;

    public String getNim() {
        return nim;
    }

    public String getNama() {
        return nama;
    }

    public Mahasiswa(String nim, String nama, String matakuliah, int semester, double nilai) {
        super(nilai);
        this.nim = nim;
        this.nama = nama;
        this.semester = semester;
        this.matakuliah = matakuliah;
        this.nilai = nilai;
    }

    public double getNilai() {
        return nilai;
    }

    public int getSemester() {
        return semester;
    }

    public String getMatakuliah() {
        return matakuliah;
    }

    public static String hasilIPK(Stream<Mahasiswa> allMahasiswa) {
        double ipk = allMahasiswa
                        .mapToDouble(Mahasiswa::getNilai)
                        .average()
                        .orElse(0.00);
        return String.format("%.2f", ipk);
    }
}
